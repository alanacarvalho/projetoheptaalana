Vue.prototype.$bus = new Vue({});
var produto = new Vue({
	el:"#produto",
    data: {
        listaFabricantes: [],
        produto:{
        	id:'',
        	nome:'',
        	fabricante:'',
        	volume:'',
        	unidade:'',
        	estoque:''
        },
        
        
    },
    created: function(){
        let vm =  this;
        vm.listarFabricantes();
        
        this.$bus.$on('produtoAtualiza', function(produto) {
        	  this.produto = produto;
        	});
        
        vm.titulo = this.produto.id ? "Alteração de produto" : "Cadastro de produto" ;
    },
    methods:{
    	listarFabricantes: function(){
			const vm = this;
			axios.get("/mercado/rs/fabricante")
			.then(response => {vm.listaFabricantes = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar os Fabricantes");
			}).finally(function() {});
		},
		
		salvar: function(){
			if(!this.produto.id){
				axios.post("/mercado/rs/produtos", this.produto)
				.then(response => {
					this.produto = {};
					alert("Produto Cadastrado com Sucesso");
					window.location.href = "/mercado/index.html";
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar os Fabricantes");
				}).finally(function() {});
			}else{
				axios.put("/mercado/rs/produtos/" + this.produto.id)
				.then(response => {
					this.produto = {};
					alert("Produto Alterado com Sucesso");
					window.location.href = "/mercado/index.html";
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar os Fabricantes");
				}).finally(function() {});
			}
		},
    }
});