Vue.prototype.$bus = new Vue({});
var inicio = new Vue({
	el:"#inicio",
    data: {
        listaProdutos: [],
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		]
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
    },
    methods:{
        buscaProdutos: function(){
			const vm = this;
			axios.get("/mercado/rs/produtos")
			.then(response => {vm.listaProdutos = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {});
		},
		
		editar: function(produto){
			
			this.$bus.$emit('produtoAtualiza', produto);
			
			window.location.href = "/mercado/cadastra-atualiza-produto.html";
		},
		
		remover: function(produto){
			
			let pro = produto.nome;
			
			if (confirm('Deseja realmente deletar '+ pro + '?')) {
				
				axios.delete("/mercado/rs/produtos/" + produto.id)
				.then(response => {
					document.location.reload(true);
					alert('O ' + pro  +' foi deletado com sucesso');
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function() {});
			}
		},
    }
});